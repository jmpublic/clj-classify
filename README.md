# clj-classify

A clojure classification library. Written for educational purposes but
might be useful to someone.

In beta and changing frequently at the moment.

Install as:

```clojure
[clj-classify "0.1.1"]
```

Require:

```clojure
(:require [clj-classify :as rec])
```

## Usage

The library defines a few data structures that can act as
classifiers. In general when querying the query should be in the same
format as the underlying data structure used by the classifier. In
theory it should be easy to produce classifiers with different
underlying data (i.e. databases) by programming at the protocols
defined in the library.

### knn classifier

The function `knn-classifier` returns a knn classifier that can be
used with the various functions in clj-classify as well as
`nearest-neighbours` from the clj-recommendation library. It expects
data in the form of a collection of vectors, the first element being
the class or key, the second a vector of attribute values and the
third an array of comments. Attribute values should be
numeric. Keyword arguments allow the normalisation method
(:mod-standard-score, :standard-score, :min-max or false for no
normalisation), the distance method (:euclidian, :manhattan, :cosine
and :pearson) and k (default 5) to be specified. The `classify` and
`n-fold-validation` functions can be used with all
classifiers. `print-confusion-table` and `kappa-stat` can be used on
the output of `n-fold-validation`. If the class or key is numeric a
numeric value will be returned using weightings from k nearest
neighbours. Numeric keys do not return a hash compatible with
`print-confusion-table`. If not a number than the most represented
class in k nearest neighbours will be returned or a guess between the
two most abundant classes in the case of a tie:

```clojure
user> athletesTrainingSet
[["Gymnastics" [54 66] ["Asuka Teramoto"]] ["Basketball" [72 162] ["Brittainey Raven"]]
 ["Basketball" [78 204] ["Chen Nan"]] ["Gymnastics" [49 90] ["Gabby Douglas"]] ... ]
user> (def kc (knn-classifier athletesTrainingSet :norm :mod-standard-score
:dist-method :manhattan :k 5))
#'clj-recommendation.core-test/kc
user> (nearest-neighbours kc ["query" [70 140] []])
(["Basketball" 0.6916752312435765] ["Basketball" 0.6916752312435765]
 ["Track" 0.5125666412795126] ["Basketball" 0.5025318917969216]
 ["Track" 0.4976604298843575])
user> (classify kc ["query" [70 140] []])
"Basketball"
user> (n-fold-validation kc)
{"Gymnastics" {"Gymnastics" 4, "Basketball" 0, "Track" 2}, "Basketball" 
{"Basketball" 7, "Gymnastics" 0, "Track" 0}, "Track"
 {"Track" 7, "Basketball" 0, "Gymnastics" 0}}
user> (-> (n-fold-validation kc) print-confusion-table)
              Gymnastics  Basketball  Track
  Gymnastics           4           0      2
  Basketball           0           7      0
       Track           0           0      7

90.0 percent correct.
Total instances: 20
nil
user> (-> (n-fold-validation kc) kappa-stat)
0.8484848484848485
user> numeric-example
[(1 [54 66] ["Asuka Teramoto"]) (2 [72 162] ["Brittainey Raven"])
 (3 [78 204] ["Chen Nan"]) (4 [49 90] ["Gabby Douglas"])
 (5 [65 99] ["Helalia Johannes"]) ... ]
user> (def kc (knn-classifier numeric-example))
#'clj-recommendation.core-test/kc
user> (classify kc ["query" [70 40]])
15.179079417746587
user>
```

### Bayesian classifier

The function `bayesian-classifer` returns a Bayesian classifier. It
expects data in the same format as the knn classifier, although
attribute values can be numerical or categorical. Conditionals are
calculated using a probability density function for numerical
attributes and frequencies for categorical attributes:

```clojure
user> (def bc (bayesian-classifier pima-train))
#'clj-recommendation.core-test/bc
user> (classify bc (first pima-test))
"0"
user> (-> (n-fold-validation bc) print-confusion-table)
      1   0
  1  22  14
  0  12  41

70.78651685393258 percent correct.
Total instances: 89
nil
user> (-> (test-classifier bc pima-test) print-confusion-table)
     0  1
  0  4  2
  1  2  3

63.63636363636363 percent correct.
Total instances: 11
nil
user>
```

### Bayesian text classifier

The function `text-classifer` returns a Bayesian text classifier. Data
should be organised into a training directory and an optional test
directory which the classifer takes as arguments. In each directory,
nested directories should represent classes and within these
directories the documents to be used for training and testing. Stop
file should be a path to a file of words, one to a line, or nil if no
stop words are used:

```clojure
user> (def tc (text-classifier "/path/polarity/stopwords174.txt" "/path/polarity/training"))
#'clj-recommendation.core-test/tc
user> (-> (n-fold-validation bc) print-confusion-table)
       pos  neg
  pos  784  216
  neg  157  843

81.35 percent correct.
Total instances: 2000
nil
user> (def tc (text-classifier "/path/to/stopwords174.txt" "/path/totrain/"
"/path/to/test"))
#'clj-recommendation.core/tc
user> (-> (test-classifier tc (test-data tc)) print-confusion-table)
... large confusion table edited ...

80.33722782793416 percent correct.
Total instances: 7532
nil
user>
```

## License

Copyright © 2017 Jason Mulvenna

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
