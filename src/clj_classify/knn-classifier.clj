(in-ns 'clj-classify.core)

(defrecord knnClassifier [data dist-method k norm]

  rec/Data

  (data-function [o f]
    (f data))

  (sorted-keys [o]
    (->> (map first data) set sort))

  (get-key [o k]
    (->> (filter #(= k %) data)
         first))

  (get-key-key [o k1 k2]
    (-> (rec/get-key data k1) (nth k2)))

  (query-attributes [o q]
    (second q))

  (query-key [o q]
    (first q))

  (distance-vector [o k1 k2]
    (vector-format (rec/query-attributes o k1) (rec/query-attributes o k2)))

  rec/Neighbours

  (calc-distance [n k1 k2]
    (rec/neighbour-dist (rec/distance-vector n k1 k2) dist-method))

  (k-items [n] k)

  (distance-method [n] dist-method)

  rec/Normalising

  (reassemble [c ma]
    (rec/data-function c (fn [x] (map #(vector (first %1) %2 (last %1))
                                     x
                                     (-> (map first ma) ma/transpose ma/slices)))))

  (get-matrix [c]
    (-> (map (partial rec/query-attributes c) data) vec ma/array))

  (norm-method [c]
    (if (map? norm)
      (norm :method)
      norm))

  (normalised? [c] (if (map? norm) norm))

  (normalise-query [c q]
    (if-let [vc (rec/normalised? c)]
      (if (= (count (rec/query-attributes c q)) (count (vc :values)))
        [(rec/query-key c q)
         (-> (mapcat #(apply rec/normalise-vector (rec/norm-method c) [%1] %2)
                     (rec/query-attributes c q) (vc :values))
             vec)
         (query-comments c q)]
        (throw
         (Exception. "Target vector column mismatch with classifier columns.")))
      q))

  Classifiers

  (new-classifier [c d]
    (-> (assoc c :data d)
        (assoc :norm false)))

  (ranked-classes [c i]
    (rec/nearest-neighbours c i))

  (select-class [c coll]
    (if-not (number? (first (first coll)))
      (->> (group-by first coll)
           vals
           (sort-by count >)
           first
           (sort-by second >)
           first
           first)
      (let [sumd (reduce #(+ %1 (second %2)) 0 coll)
            influences (map #(/ (second %) sumd) coll)]
        [(->> (map #(* (first %1) %2) coll influences)
              (reduce +))])))

  (query-comments [c q]
    (last q))

  (mark-classification [c q p]
    (if (number? p)
      {(rec/query-key c q) {:predicted p :delta (- p (rec/query-key c q))}}
      (let [ocs (remove #{p} (rec/sorted-keys c))]
        {(rec/query-key c q) (reduce (fn [a b] (merge a {b 0})) {p 1} ocs)})))
  
  (stratify [c n]
    (cond (<= n 1)
          (throw (Exception. "1 or 0 doesn't make sense for n-times stratification."))
          :else
          (assoc c :stratified (zipmap (range 1 (+ 1 n))
                                       (rec/data-function c (partial stratify-data n))))))

  (get-other-strata [c k]
    (->> (dissoc (:stratified c) k) vals (apply concat)))
  
  (get-strata [c k]
    ((:stratified c) k))
  
  (mappable-strata [c]
    (keys (:stratified c))))

