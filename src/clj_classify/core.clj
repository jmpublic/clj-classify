(ns clj-classify.core
  (:require [clojure.set :as set]
            [clj-recommendation.core :as rec]
            [clojure.core.reducers :as r]
            [clojure.core.matrix :as ma]
            [clojure.core.matrix.operators :as mao]
            [clojure.set :as set]
            [clojure.pprint :as pp]
            [clojure.java.io :as io]
            [clojure.string :as st]
            [me.raynes.fs :as fs]
            [iota :as ia]
            [clojure.edn :as edn]))

(load "protocols")
(load "utilities")
(load "confusion")
(load "knn-classifier")
(load "bayesian-classifier")
(load "text-classifier")
(declare normalise)

;; init functions

(defn knn-classifier
  "Returns a knnClassifier. Data should be a collection of vectors the
  first element of which is the class, the second another vector of
  attribute values and the third a vector of comments. Allowable
  values for dist-method keyword are :euclidean, :manhattan, :pearson
  and :cosine. Default k value is 5."
  [data & {:keys [norm dist-method k]
           :or {norm :mod-standard-score dist-method :manhattan k 5}}]
  (let [c (->knnClassifier data dist-method k norm)]
    (if norm (normalise c) c)))

(defn bayesian-classifier
  "Returns a Bayesian classifier. Data should be a collection of
  vectors with the first element the class, the second element a
  vector of attributes and the third element a vector of
  comments. Items to be classified should be a vector with the same
  format. Uses probability density function for numeric attributes. If
  data is a lazy list it will be realised while calculating priors and
  conditionals."
  [data]
  (let [c (->bayesianClassifier data)]
    (-> (assoc c :priors (calc-priors c))
        (assoc :conditionals (calc-conditionals c)))))

(defn text-classifier
  "Returns a text classifier. Data should be organised into a training
  directory and test directory. In each, nested directories should
  represent classes and within these directories the documents to be
  used for training and testing. Stop file should be a path to a file
  of words, one to a line, or nil if no stop words are used."
  ([stop-file data-dir] (text-classifier stop-file data-dir nil))
  ([stop-file data-dir test-dir]
   (let [stops (read-stops stop-file)
         c (->textClassifier (read-stops stop-file) data-dir test-dir)]
     (-> (assoc c :priors (calc-priors c))
         (assoc :conditionals (calc-conditionals c))))))

;; api

(defn normalise
  "Normalises a classifier."
  [r]
  (let [n (->> (rec/get-matrix r)
               ma/transpose
               ma/slices
               (map #(rec/normalise-vector (rec/norm-method r) (vec %))))]
    (assoc r :data (rec/reassemble r n) :norm {:method (rec/norm-method r) :values (mapv second n)})))

(defn test-data
  "Returns a collection of test data from a text classifier for use
  with the 'test-classifier' function."
  [classifier]
  (mapcat (fn [x]
            (map (fn [y] [(fs/base-name x) y])
                 (fs/list-dir x)))
          (fs/list-dir (:test-data classifier))))

(defn classify
  "Classifys an item using a classifier. Returns a single value
  representing the class."
  [c item]
  (->> (ranked-classes c item) (select-class c)))

(defn potential-classifications
  "Returns a list of vectors representing potential classes for the
  query item. Vectors have the class as the first element and a score
  as the second."
  [c item]
  (ranked-classes c item))

(defn test-classifier
  "Takes a classifier and a collection of objects to be
  classified. The collection contains vectors, the first element of
  which is the true classification and the second the item to be
  classified in whatever format the classifier expects (it will be
  processed using 'process-query' from the Classifiers
  protocol). Returns a hash of hashes representing a confusion table."
  [classifier coll]
  (let [r (map (fn [y]
                 (->> (classify classifier y) (mark-classification classifier y)))
               coll)]
    (apply merge-with (fn [x y] (merge-with + x y)) r)))

(defn n-fold-validation
  "Performs n-fold (default 10) validation on any object implementing
  the Classifiers protocol. Returns a hash of hashes representing a
  confusion table."
  ([classifier] (n-fold-validation classifier 10))
  ([classifier n]
   (let [sc (stratify classifier n)]
     (let [r (->> (map (fn [x]
                         (let [tc (new-classifier classifier (get-other-strata sc x))]
                           (test-classifier tc (get-strata sc x))))
                       (mappable-strata sc))
                  (apply merge-with (fn [x y] (merge-with + x y))))]
       r))))

;; (defn strata->file
;;   [classifier test train]
;;   "Outputs first strata to the file specified as test and the rest to
;;   the file specified as train."
;;   (let [sc (stratify classifier 10)]
;;     (with-open [w (io/writer test)]
;;       (dorun (map #(.write w (print-format classifier %))
;;                   ((:stratified sc) 1))))
;;     (with-open [w (io/writer train)]
;;       (dorun (map #(.write w (print-format classifier %))
;;                   (apply concat (-> (dissoc (:stratified sc) 1) vals)))))))

