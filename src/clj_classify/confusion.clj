(in-ns 'clj-classify.core)

(defn correct
  "Returns number of correct predictions in confusion table."
  [confusion]
  (->> (map (fn [[k v]] (get-in confusion [k k] 0)) confusion)
       (reduce +)))

(defn total-instances
  "Returns the total number of classifications made."
  [confusion]
  (->> (map val confusion)
       (mapcat vals)
       (reduce +)))

(defn correct-proportion
  "Returns the proportion of correct classifications."
  [confusion]
  (/ (correct confusion) (total-instances confusion)))

(defn predicted-class-totals
  "Returns a hash of predicted class totals."
  [confusion]
  (->> (map val confusion)
       (apply merge-with +)))

(defn actual-class-totals
  "Returns a hash of actual class predictions."
  [confusion]
  (->> (map (fn [[k v]] {k (->> (vals v) (reduce +))}) confusion)
       (apply merge)))

(defn predicted-class-proportions
  "Returns predicted class proportions."
  [confusion]
  (let [ct (predicted-class-totals confusion)
        tots (total-instances confusion)]
    (reduce (fn [x y]
              (assoc x y (/ (ct y) tots)))
            {}
            (keys ct))))

(defn print-confusion-table
  "Prints a confusion table."
  [confusion]
  (let [cls (->> (vals confusion) first keys)
        rows (cons (vec (cons "" cls))
                   (map (fn [x] (->> (map (fn [y]
                                           (get-in confusion [x y] 0))
                                         cls)
                                    (cons x)
                                    vec))
                        cls))
        cs (->> (apply mapv vector rows)
                (map #(map (comp count str) %))
                (map #(apply max %))
                (map #(+ % 2)))]
    (dorun (for [r rows]
             (->> (map #(format (str "%" %2 "s") %1) r cs)
                  (apply str)
                  println)))
    (println)
    (println (str (* (correct-proportion confusion) 100.0)
                  " percent correct."))
    (println (str "Total instances: " (total-instances confusion)))))

(defn kappa-stat
  "Calculates kappa-stat of a confusion table."
  [confusion]
  (let [cls (->> (vals confusion) first keys)
        acp (actual-class-totals confusion)
        pcp (predicted-class-proportions confusion)
        rand-cor (correct-proportion
                  (reduce (fn [x y]
                            (assoc x y
                                   (->> (map (fn [z]
                                               {z (* (get acp y 0)
                                                     (get pcp z 0))})
                                             cls)
                                        (apply merge))))
                          {} cls))]
    (double (/ (- (correct-proportion confusion) rand-cor)
               (- 1 rand-cor)))))

