(in-ns 'clj-classify.core)

(defrecord bayesianClassifier [data]

  rec/Data

  (data-function [o f]
    (f data))

  (sorted-keys [o]
    (or (-> (keys (:priors o)) sort seq)
        (->> (map first data) set sort)))

  (get-key [o k]
    (->> (filter #(= k (first %)) data)
         first))

  (get-key-key [o k1 k2]
    (->> (rec/get-key o k1) (nth k2)))

  (query-attributes [o q]
    (second q))

  (query-key [o q]
    (first q))

  (query-comments [o q]
    (last q))

  Classifiers

  (new-classifier [c d]
    (let [new (merge c (->bayesianClassifier d))]
      (-> (assoc new :priors (calc-priors new))
          (assoc :conditionals (calc-conditionals new)))))

  (ranked-classes [c i]
    (->> (map (fn [x]
                [x (->> (conj
                         (conditionals c x (rec/query-attributes c i))
                         (prior c x))
                        ma/array
                        (ma/ereduce (calc-function c)))])
              (rec/sorted-keys c))
         (sort-by second >)))

  (select-class [c coll]
    (-> (first coll) first))

  (mark-classification [c q p]
    (let [ocs (remove #{p} (rec/sorted-keys c))]
      {(rec/query-key c q) (reduce (fn [a b] (merge a {b 0})) {p 1} ocs)}))
  
  (stratify [c n]
    (cond (<= n 1)
          (throw (Exception.
                  "1 or 0 doesn't make sense for n-times stratification."))
          :else
          (assoc c :stratified (zipmap (range 1 (+ 1 n))
                                       (rec/data-function c (partial stratify-data n))))))

  (get-other-strata [c k]
    (->> (dissoc (:stratified c) k) vals (apply concat)))
  
  (get-strata [c k]
    ((:stratified c) k))
  
  (mappable-strata [c]
    (keys (:stratified c)))

  bayesianClassifiers

  (calc-priors [c]
    (vector->priors (:data c)))

  (calc-conditionals [c]
    (->> (map (fn [cl]
                (let [d (->> (filter #(= cl (first %)) data)
                             (map (partial rec/query-attributes c))
                             (apply mapv vector)
                             (map (partial vector->conditional) (iterate inc 1))
                             (apply merge))]
                  {cl (fn [in ie] ((d in) ie))}))
              (rec/sorted-keys c))
         (apply merge)))

  (prior [c i]
    ((:priors c) i))

  (conditionals [c cl i]
    (mapv (fn [in ie]
            (((:conditionals c) cl) in ie))
          (iterate inc 1) i))

  (calc-function [c]
    *))
