(in-ns 'clj-classify.core)

(defn ssd
  [v]
  (let [[co] (ma/shape v)
        m (/ (-> (ma/array v) ma/esum) co)]
    [m (Math/sqrt (/ (-> (ma/emap #(- % m) v) rec/sum-squares) (- co 1)))]))

(defn- pdf
  [m s x]
  (let [ep (Math/pow Math/E (/ (* -1 (Math/pow (- x m) 2))
                               (* 2 (Math/pow s 2))))]
    (* (/ 1 (* (Math/sqrt (* 2 Math/PI)) s)) ep)))

(defn- vector-format
  [h1 h2]
  (-> (ma/array [h1 h2]) ma/transpose))

(defn- stratify-data
  [n h]
  (let [c (if (number? (first (first h)))
            (->> (sort-by first h)
                 (partition-all (/ (count h) 4)))
            (->> (group-by first h)
                 vals))]
    (->> (map shuffle c)
         (map #(partition-all
                (+ 1 (quot (count %) n))
                %))
         (map #(zipmap (range 1 (+ 1 n)) %))
         (apply merge-with concat)
         vals
         vec)))

(defn- vector->conditional
  "Takes a vector and an index number and returns a hash with the
  index as key and the value a function that takes a single argument
  and returns the conditional for that argument. If the first element
  of the vector is numeric the function will be the probability
  density function (it is assumed all other values are also numbers)
  and if anything else then the function uses frequencies to calculate
  conditionals."
  [in v]
  (if (number? (first v))
    (let [[mean ssd] (ssd (ma/array v))]
      {in (fn [ie] (pdf mean ssd ie))})
    (let [co (count v)
          fs (frequencies v)
          cc (count (keys fs))]
      {in (let [r (->> (map (fn [c]
                              (let [nc (get fs c 0)]
                                [c (/ (+ nc (* (/ 1 cc) cc))
                                      (+ cc co))]))
                            (keys fs))
                       (into {}))]
            (fn [ie]
              (get r ie 0)))})))

(defn- vector->priors
  [c]
  "Takes a collection of vectors and returns a hash of prior
  probabilities based on the frequencies of the first element of each
  vector."
  (let [co (count c)]
    (->> (map #(first %) c)
         frequencies
         (map (fn [[k v]] [k (/ v co)]))
         (into {}))))
