(in-ns 'clj-classify.core)

(defn- count-map
  [stops s]
  (reduce (fn [m w]
            (let [ws (re-seq #"[a-z']+" (.toLowerCase w))]
              (reduce #(if (stops %2)
                         (update-in %1 [%2] (fnil inc 0))
                         %1)
                      m ws)))
          {}
          (st/split s #" ")))

(defn- add-maps
  ([] {})
  ([m1 m2]
   (reduce (fn [m [k v]] (update-in m [k] (fnil (partial + v) 0))) m1 m2)))

(defn- read-words-file
  [stops file]
  (->> (ia/seq (str file))
       (r/filter identity)
       (r/map (partial count-map stops))
       (r/fold add-maps)))

(defn- read-words
  [stops coll]
  (->> (pmap (partial read-words-file stops) coll)
       (reduce (partial merge-with +))))

(defn- read-stops
  [file]
  (if file
    (with-open [r (io/reader file)]
      (-> (line-seq r) set (conj "") complement))
    (complement #{""})))

(defrecord textClassifier [stops data test-data]

  rec/Data

  (data-function [o f]
    (f data))

  (sorted-keys [o]
    (or (keys (:priors o))
        (->> (fs/list-dir data)
             (map fs/base-name))))

  (get-key [o k]
    (if (map? data)
      (data k)
      (fs/list-dir (fs/file data k))))

  (get-key-key [o k1 k2]
    (fs/file (rec/get-key o k1) k2))

  (query-attributes [o q]
    (read-words-file stops (second q)))

  (query-key [o q]
    (first q))

  (query-comments [o q]
    nil)

  Classifiers

  (new-classifier [c d]
    (let [nc (assoc c :data d)]
      (-> (assoc nc :priors (calc-priors nc))
          (assoc :conditionals (calc-conditionals nc)))))

  (ranked-classes [c i]
    (->> (pmap (fn [x]
                 [x (->> (conj
                          (conditionals c x (rec/query-attributes c i))
                          (prior c x))
                         ma/array
                         (ma/ereduce (calc-function c)))])
               (rec/sorted-keys c))
         (sort-by second >)))

  (select-class [c coll]
    (-> (first coll) first))

  (mark-classification [c q p]
    (let [ocs (remove #{p} (rec/sorted-keys c))]
      {(rec/query-key c q) (reduce (fn [a b] (merge a {b 0})) {p 1} ocs)}))

  (stratify [c n]
    (if-not (:stratified c)
      (assoc c :stratified
             (->> (pmap (fn [cl]
                          (let [files (-> (rec/get-key c cl) shuffle)]
                            (zipmap (range 1 (+ 1 n))
                                    (->> (partition-all (+ 1 (quot (count files) n)) files)
                                         (map (fn [x] {cl (vec x)}))))))
                        (rec/sorted-keys c))
                  (apply merge-with merge)))
      c))

  (get-other-strata [c k]
    (->> (dissoc (:stratified c) k)
         vals
         (apply merge-with #(reduce conj %1 %2))))

  (get-strata [c k]
    (->> (mapcat (fn [[k2 v]]
                   (mapv (fn [file] [k2 file]) v))
                 ((:stratified c) k))))

  (mappable-strata [c]
    (keys (:stratified c)))

  bayesianClassifiers

  (calc-priors [c]
    (let [data (:data c)
          counts (->> (map (fn [x] [x (count (rec/get-key c x))])
                           (rec/sorted-keys c)))
          total (reduce + (map second counts))]
      (->> (map (fn [[k v]] [k (/ v total)]) counts)
           (into {}))))

  (prior [c i]
    (Math/log10 ((:priors c) i)))

  (calc-conditionals [c]
    (let [wfs (->> (pmap (fn [cl]
                           {cl (rec/data-function c (fn [_] (read-words stops (rec/get-key c cl))))})
                         (rec/sorted-keys c))
                   (reduce merge))
          hcos (->> (map (fn [[k v]] {k (reduce + (vals v))}) wfs) (reduce merge))
          voc (->> (vals wfs)
                   (reduce (partial merge-with +))
                   (filter (fn [[k v]] (> v 2)))
                   (map first)
                   set)
          vc (count voc)]
      (->> (pmap (fn [w]
                  {w (->> (map (fn [h] {h (/ (+ 1 (get-in wfs [h w] 0))
                                            (+ vc (hcos h)))})
                               (rec/sorted-keys c))
                          (reduce merge))})
                voc)
           (reduce merge))))

  (conditionals [c cl i]
    (mapv (fn [[k v]]
            (* v (let [r (get-in (:conditionals c) [k cl] 0)]
                   (if-not (= r 0) (Math/log10 r) 0))))
          i))

  (calc-function [c]
    +))
