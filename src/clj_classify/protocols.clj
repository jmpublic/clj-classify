(in-ns 'clj-classify.core)

(defprotocol Classifiers
  (new-classifier [c d] "Returns a new classifier with same attributes
  as c except contains data (d). Normalisation data, if any, is also
  removed.")
  (ranked-classes [c i] "Returns a list of ranked potential
  classifications. The list contains vectors with the class as the
  first element and a score as the second.")
  (select-class [c coll] "Selects a class from the ranked list of
  potential classes returned by 'ranked-classes'. Returns the class
  selected.")
  (query-comments [c q] "Returns any comments included in the query
  item.")
  (mark-classification [c q p])
  (stratify [c n])
  (get-other-strata [c k])
  (get-strata [c k])
  (mappable-strata [c]))

(defprotocol bayesianClassifiers
  (calc-priors [c] "Calculate priors.")
  (calc-conditionals [c] "Calculate conditionals.")
  (prior [c i] "Returns the prior probability for item i.")
  (conditionals [c cl i] "Returns a vector of conditional
  probabilities for class (cl) given an item to be classified.")
  (calc-function [c] "Returns function used to sum probabilities
  during classification. '*' for standard probabilities or '+' for
  when using log probabilities."))
