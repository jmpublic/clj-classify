(ns clj-classify.core-test
  (:require [clojure.test :refer :all]
            [clj-classify.core :refer :all]
            [clojure.java.io :as io]
            [clojure.edn :as edn]
            [clj-recommendation.core :as rec]))

(defonce athletesTrainingSet (edn/read-string (slurp (io/resource "athletesTrainingSet.clj"))))
(defonce iris-tr (edn/read-string (slurp (io/resource "irisTrainingSet.clj"))))
(defonce iris-te (edn/read-string (slurp (io/resource "irisTestSet.clj"))))
(defonce pima-test (edn/read-string (slurp (io/resource "pima-test.clj"))))
(defonce pima-train (edn/read-string (slurp (io/resource "pima-train.clj"))))

(deftest normalisation
  (testing "Normalisation"
    (let [tm [75000 55000 45000 115000 70000 105000 69000 43000]]
      (is (= (-> (rec/normalise-vector :min-max tm) first)
             [0.4444444444444444 0.1666666666666667
              0.02777777777777778 1.0 0.375 0.861111111111111
              0.3611111111111111 0.0]))
      (is (= (-> (rec/normalise-vector :standard-score tm) first)
             [0.11714129261815952 -0.6977546560299067
              -1.1052026303539397 1.746933189914292
              -0.08658269454385703 1.3394852155902588
              -0.12732749197626034 -1.1866922252187464]))
      (is (= (-> (rec/normalise-vector :mod-standard-score tm) first)
             [0.2875816993464052 -0.7581699346405228
              -1.2810457516339868 2.3790849673202614
              0.026143790849673203 1.8562091503267975
              -0.026143790849673203 -1.3856209150326797])))))

(deftest default-classifier-test
  (testing "Default classifier"
    (let [atr (knn-classifier athletesTrainingSet :k 1)]
      (is (= (-> (:data atr) first second)
             [-1.9327731092436975 -1.2184249628528976]))
      (is (= (rec/normalise-query atr ["query" [70 140] []])
             ["query" [0.7563025210084033 0.9806835066864785] []]))
      (is (= (rec/nearest-neighbours atr ["query" [70 140] []])
             '(["Basketball" 0.6916752312435765])))
      (is (= (classify atr ["query" [70 140] []])
             "Basketball")))))

(deftest bc-test
  (testing "Bayesian classifier"
    (let [bc (bayesian-classifier pima-train)]
      (is (= (test-classifier bc pima-test)
             {"1" {"0" 2, "1" 3}, "0" {"0" 4, "1" 2}})))))
